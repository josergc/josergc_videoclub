package josergc.videoclub.model.db;

import java.util.List;
import java.util.UUID;

import josergc.videoclub.model.Article;
import josergc.videoclub.model.Friend;
import josergc.videoclub.model.WhatArticleWasBorrowedToWho;
import android.content.ContentValues;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public abstract class Datasource {

	private static final String ID_FIELD = "id";
	private static final String TITLE_FIELD = "title";
	private static final String IS_BOOK_FIELD = "is_book";
	private static final String ARTICLES_TABLE = "articles";
	private static final String PHONE_FIELD = "phone";
	private static final String NAME_FIELD = "name";
	private static final String FRIENDS_TABLE = "friends";
	private static final String DATABASE_NAME = "VideoclubDB";
	private static final String ANY = "*";

	/**
	 * Returns a friend by a given id
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public abstract Friend getFriend(UUID id) throws SQLException;

	/**
	 * Looks for friends using the given filters
	 * 
	 * @param nameFilter
	 * @param phoneFilter
	 * @return a {@link Cursor}
	 * @throws SQLException
	 */
	public abstract Cursor getFriendsDB(String nameFilter,
			String phoneFilter) throws SQLException;

	/**
	 * Looks for friends using the given filters
	 * 
	 * @param nameFilter
	 * @param phoneFilter
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Friend> getFriends(String nameFilter,
			String phoneFilter) throws SQLException;

	/**
	 * Saves the given friend
	 * 
	 * @param f
	 * @throws SQLException
	 */
	public abstract void save(Friend f) throws SQLException;

	/**
	 * Deletes the given friend
	 * 
	 * @param f
	 * @throws SQLException
	 */
	public abstract void delete(Friend f) throws SQLException;

	/**
	 * Returns an article by a given id
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public abstract Article getArticle(UUID id) throws SQLException;

	/**
	 * Looks for articles using the given filters
	 * 
	 * @param isBookFilter
	 * @param titleFilter
	 * @return
	 * @throws SQLException
	 */
	public abstract List<Article> getArticles(Boolean isBookFilter,
			String titleFilter) throws SQLException;

	/**
	 * Saves the given article
	 * 
	 * @param article
	 * @throws SQLException
	 */
	public abstract void save(Article article) throws SQLException;

	/**
	 * Deletes the given article
	 * 
	 * @param article
	 * @throws SQLException
	 */
	public abstract void delete(Article article) throws SQLException;

	/**
	 * 
	 * @param friendFilter
	 * @param articleFilter
	 * @return
	 * @throws SQLException
	 */
	public abstract List<WhatArticleWasBorrowedToWho> getWhatArticleWasBorrowedToWho(
			List<Friend> friendFilter, List<Article> articleFilter)
			throws SQLException;

	/**
	 * Get an instance of the {@link Datasource} from the given
	 * {@link ContextWrapper}
	 * 
	 * @param cw
	 * @return
	 * @throws SQLException
	 */
	public static Datasource getInstance(final ContextWrapper cw)
			throws SQLException {
		final SQLiteDatabase db = cw.openOrCreateDatabase(DATABASE_NAME,
				ContextWrapper.MODE_PRIVATE, null);
		db.execSQL("create table if not exists " + FRIENDS_TABLE + "("
				+ ID_FIELD + " text primary key, " + NAME_FIELD + " text, "
				+ PHONE_FIELD + " text)");
		db.execSQL("create table if not exists " + ARTICLES_TABLE + "("
				+ ID_FIELD + " text primary key, " + IS_BOOK_FIELD
				+ " integer, " + TITLE_FIELD + " text)");
		db.execSQL("create table if not exists what_article_was_borrowed_to_who(friend_id text references friends, article_id text references articles, when_was_borrowed integer, when_was_returned integer)");
		return new Datasource() {

			@Override
			public Friend getFriend(UUID id) throws SQLException {
				Cursor cursor = db.query(DATABASE_NAME, new String[] {
						NAME_FIELD, PHONE_FIELD }, ID_FIELD + "=?",
						new String[] { id.toString() }, null, null, null);
				try {
					if (cursor.moveToNext()) {
						Friend f = new Friend();
						f.setId(id);
						f.setName(cursor.getString(1));
						f.setPhone(cursor.getString(2));
						return f;
					}
				} finally {
					cursor.close();
				}
				return null;
			}

			@Override
			public List<Friend> getFriends(String nameFilter, String phoneFilter)
					throws SQLException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void save(Friend f) throws SQLException {
				UUID id = f.getId();
				ContentValues values = new ContentValues();
				values.put(NAME_FIELD, f.getName());
				values.put(PHONE_FIELD, f.getPhone());
				if (id != null) {
					// Update
					db.update(FRIENDS_TABLE, values, ID_FIELD + "=?",
							new String[] { id.toString() });

				} else {
					// Insert
					values.put(ID_FIELD, (id = UUID.randomUUID()).toString());
					db.insert(FRIENDS_TABLE, "null", values);
					f.setId(id);
				}
			}

			@Override
			public void delete(Friend f) {
				db.delete(FRIENDS_TABLE, ID_FIELD + "=?", new String[] { f
						.getId().toString() });
			}

			@Override
			public Article getArticle(UUID id) {
				Cursor cursor = db.query(DATABASE_NAME, new String[] {
						IS_BOOK_FIELD, TITLE_FIELD }, ID_FIELD + "=?",
						new String[] { id.toString() }, null, null, null);
				try {
					if (cursor.moveToNext()) {
						Article f = new Article();
						f.setId(id);
						f.setBook(cursor.getInt(1) != 0);
						f.setTitle(cursor.getString(2));
						return f;
					}
				} finally {
					cursor.close();
				}
				return null;
			}

			@Override
			public List<Article> getArticles(Boolean isBookFilter,
					String titleFilter) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void save(Article f) {
				UUID id = f.getId();
				ContentValues values = new ContentValues();
				values.put(IS_BOOK_FIELD, f.isBook() ? 1 : 0);
				values.put(TITLE_FIELD, f.getTitle());
				if (id != null) {
					// Update
					db.update(ARTICLES_TABLE, values, ID_FIELD + "=?",
							new String[] { id.toString() });

				} else {
					// Insert
					values.put(ID_FIELD, (id = UUID.randomUUID()).toString());
					db.insert(ARTICLES_TABLE, "null", values);
					f.setId(id);
				}
			}

			@Override
			public void delete(Article f) {
				db.delete(ARTICLES_TABLE, ID_FIELD + "=?", new String[] { f
						.getId().toString() });

			}

			@Override
			public List<WhatArticleWasBorrowedToWho> getWhatArticleWasBorrowedToWho(
					List<Friend> friendFilter, List<Article> articleFilter) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public long getNumberOfFriends() {
				return DatabaseUtils.queryNumEntries(db, FRIENDS_TABLE);
			}

			@Override
			public long getNumberOfArticles() {
				return DatabaseUtils.queryNumEntries(db, ARTICLES_TABLE);
			}

			@Override
			public Cursor getFriendsDB(String nameFilter, String phoneFilter)
					throws SQLException {
				db.query(FRIENDS_TABLE, new String[]{ NAME_FIELD}, "", selectionArgs, groupBy, having, orderBy)
				return null;
			}

		};
	}

	/**
	 * Returns the number of friends in the database
	 * 
	 * @return
	 */
	public abstract long getNumberOfFriends();

	/**
	 * Returns the number of articles in the database
	 * 
	 * @return
	 */
	public abstract long getNumberOfArticles();
}
