package josergc.videoclub.model;

import java.util.UUID;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public class Article {
	
	private UUID id;
	private boolean isBook;
	private String title;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public boolean isBook() {
		return isBook;
	}

	public void setBook(boolean isBook) {
		this.isBook = isBook;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
