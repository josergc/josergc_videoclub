package josergc.videoclub.model;

import java.util.UUID;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public class Friend {
	
	private UUID id;
	private String name;
	private String phone;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
