package josergc.videoclub.model;

import java.util.Date;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public class WhatArticleWasBorrowedToWho {

	private Article article;
	private Friend friend;
	private Date whenThisWasBorrowed;
	private Date whenThisWasReturned;

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Friend getFriend() {
		return friend;
	}

	public void setFriend(Friend friend) {
		this.friend = friend;
	}

	public Date getWhenThisWasBorrowed() {
		return whenThisWasBorrowed;
	}

	public void setWhenThisWasBorrowed(Date whenThisWasBorrowed) {
		this.whenThisWasBorrowed = whenThisWasBorrowed;
	}

	public Date getWhenThisWasReturned() {
		return whenThisWasReturned;
	}

	public void setWhenThisWasReturned(Date whenThisWasReturned) {
		this.whenThisWasReturned = whenThisWasReturned;
	}

}
