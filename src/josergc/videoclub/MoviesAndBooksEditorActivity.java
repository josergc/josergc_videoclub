package josergc.videoclub;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MoviesAndBooksEditorActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movies_and_books_editor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.movies_and_books_editor, menu);
		return true;
	}

}
