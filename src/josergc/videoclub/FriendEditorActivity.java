package josergc.videoclub;

import josergc.videoclub.R;
import josergc.videoclub.R.layout;
import josergc.videoclub.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public class FriendEditorActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friend_editor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friend_editor, menu);
		return true;
	}

}
