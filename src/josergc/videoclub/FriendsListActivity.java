package josergc.videoclub;

import josergc.videoclub.model.db.Datasource;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;

/**
 * 
 * @author josergc@gmail.com
 * @since 1.0
 */
public class FriendsListActivity extends Activity {

	private Datasource datasource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friends_list);
		try {
			datasource = Datasource.getInstance(this);
		} catch (Throwable e) {
			e.printStackTrace();
			new AlertDialog.Builder(this)
					.setMessage("Error opening datasource: " + e)
					.setTitle("ERROR").show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friends_list, menu);
		return true;
	}

}
