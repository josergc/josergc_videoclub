package josergc.videoclub;

import josergc.videoclub.R;
import josergc.videoclub.model.db.Datasource;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	Datasource datasource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			datasource = Datasource.getInstance(this);
		} catch (Throwable e) {
			e.printStackTrace();
			new AlertDialog.Builder(this)
					.setMessage("Error opening datasource: " + e)
					.setTitle("ERROR").show();
		}
	}

	public void openFriendsListActivity(View view) {
		Intent intent;
		if (datasource.getNumberOfFriends() > 0L) {
			intent = new Intent(this, FriendsListActivity.class);
		} else {
			intent = new Intent(this, FriendEditorActivity.class);
		}
		this.startActivity(intent);
	}

	public void openMoviesAndBooksListActivity(View view) {
		Intent intent;
		if (datasource.getNumberOfFriends() > 0L) {
			intent = new Intent(this, MoviesAndBooksListActivity.class);
		} else {
			intent = new Intent(this, MoviesAndBooksEditorActivity.class);
		}
		this.startActivity(intent);
	}

	public void openLendListActivity(View view) {
		Intent intent = new Intent(this, LendListActivity.class);
		this.startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
